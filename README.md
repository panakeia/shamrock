# shamrock

A Discord bot written in Haskell, using [discord.hs](https://www.stackage.org/package/discord-hs).

To run the bot:

 1. Enter your [bot's API key](https://discordapp.com/developers/applications/) and your username#discriminant in `config.yaml`.
 2. Run `stack build`.
 3. Run `stack exec shamrock-exe`.

## Commands

 - `.do` runs the message content through GHCi (imports aren't allowed, so no funny business)
