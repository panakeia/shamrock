{-# LANGUAGE OverloadedStrings, TypeOperators, FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses, DataKinds, TypeFamilies #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Main where

import Control.Monad (mzero)
import Control.Monad.IO.Class
import Data.Proxy
import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.Yaml as Y
import Data.Yaml (FromJSON(..), (.:))
import qualified Data.Maybe as M
import GHC.TypeLits

import Network.Discord

import Lib

-- Types for parsing messages in search of commands.
-- Based on https://gitlab.com/jkoike/Discord.hs/blob/dev/examples/pingpong.hs,
-- but with added support for commands with variable replies.

data Command (a :: Symbol)

instance KnownSymbol a => EventMap (Command a) (DiscordApp IO) where
  type Domain   (Command a) = Message
  type Codomain (Command a) = Message

  mapEvent p (m@Message{messageContent = c, messageAuthor = User{userIsBot = bot}})
    | bot = do
      liftIO $ putStrLn "Ignoring Bot Message"
      mzero
    | c `startsWith` T.pack (symbolVal $ commandName p) = do
      liftIO $ putStrLn "Command Match"
      return m
    | otherwise = do
      liftIO . putStrLn $ "No Command Match: " ++ show c ++ "!=" ++ symbolVal (commandName p)
      mzero
    where
      commandName :: Proxy (Command a) -> Proxy a
      commandName _ = Proxy


data Reply (a :: Symbol)

instance KnownSymbol a => EventMap (Reply a) (DiscordApp IO) where
  type Domain   (Reply a) = Message
  type Codomain (Reply a) = ()

  mapEvent p Message{messageChannel = chan, messageContent = c} = do
    symbol <- return . symbolVal . replySymbol $ p
    reply  <- liftIO . replyText $ symbol
    _      <- doFetch $ CreateMessage chan reply Nothing
    return ()
    where
      replySymbol :: Proxy (Reply a) -> Proxy a
      replySymbol _ = Proxy

      replyText :: String -> IO Text
      replyText symbol = 
        case symbol of
          "do"      -> evaluate . dropFirstWord $ c
          otherwise -> return $ T.pack symbol


-- Config is a type representing a configuration file with keys:
--   - botToken, Discord's API token for authenticating the bot
--   - owner, the username#discriminant of the owner

data Config =
  Config {
    botToken :: Text
  , owner    :: Text
  } deriving (Eq, Show)

instance FromJSON Config where
  parseJSON (Y.Object v) =
    Config <$>
    v .: "botToken" <*>
    v .: "owner"
  parseJSON _ = fail "Expected Object for Config value"


-- Implement DiscordAuth by reading auth token from config.yaml.

instance DiscordAuth IO where
  auth = do
    config <- Y.decodeFileEither "config.yaml"
    case config of
      Left err                  -> fail "Failed to parse config.yaml"
      Right (Config botToken _) -> return $ Bot $ T.unpack botToken
  version = return "0.0.1"
  runIO   = id


-- Define command/reply pairs.

type ShamrockApp =
  (
    (MessageCreateEvent :<>: MessageUpdateEvent) :>
      (    (Command ".ping" :> Reply "pong")
      :<>: (Command ".pong" :> Reply "ping")
      :<>: (Command ".do"   :> Reply "do")
      )
  )

instance EventHandler ShamrockApp IO


-- Main program.

main :: IO ()
main = runBot (Proxy :: Proxy (IO ShamrockApp)) 
