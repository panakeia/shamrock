{-# LANGUAGE OverloadedStrings #-}

module Lib
    ( startsWith
    , firstWord
    , dropFirstWord
    , evaluate
    ) where

import qualified Data.Text as T
import Data.Text (Text)
import qualified Data.List as L
import qualified Language.Haskell.Interpreter as I
import Language.Haskell.Interpreter (InterpreterT, Extension(..), OptionVal ((:=)))

-- Determine if the sentence starts with a given word.
startsWith :: Text -> Text -> Bool
startsWith = flip (L.isPrefixOf . return) . T.words

-- Return the first word from a section of text.
firstWord :: Text -> Text
firstWord = head . T.words

-- Remove the first word from a section of text.
dropFirstWord :: Text -> Text
dropFirstWord = T.unwords . L.drop 1 . T.words

-- Import the Prelude, but prevent any further imports.
interpreterOpts :: InterpreterT IO ()
interpreterOpts = do
    I.set [I.languageExtensions := [NoPackageImports]]
    I.setImports ["Prelude"]

-- Run a string through the GHCi interpreter.
evaluate :: Text -> IO Text
evaluate expr = do
  x <- I.runInterpreter $ interpreterOpts >> I.eval (T.unpack expr)
  return $ case x of
    Left  err -> "sorry, i couldn't run that ;_;"
    Right res -> T.pack res
